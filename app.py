import os
import time
import json
from flask import render_template,send_file,Flask
app = Flask(__name__)



class FileDB:
    def __init__(self, name):
        self.name = "./"+name
        try:
            file = open(self.name, 'r')
        except IOError:
            file = open(self.name, 'w')
        file.close()

    def read(self):
        with open(self.name, "r") as filequeue:
            data = filequeue.read().split("\n")
            return [x for x in data if x!=""]

    def append(self, d):
        with open(self.name, "r+") as filequeue:
            data = filequeue.read().split("\n")
            data.append(d)
            filequeue.write(d+"\n")

    def uappend(self, d):
        with open(self.name, "r+") as filequeue:
            data = filequeue.read().split("\n")
            data = [x for x in data if x!=""]
            if d not in data:
                filequeue.write(d+"\n")
                return 1
            else:
                return 0

BASE_PATH = "./models/"

@app.route('/')
def index():
	return send_file("index.html")

@app.route('/<model>/add',methods=["POST"])
def index21(mode_id):
    msg = request.json.get("msg")
    tmp = FileDB(BASE_PATH+model)
    if msg!=None and msg:
        return "{'success': "+tmp.uappend(msg)+"}"
    else:
        return "{'error':'empty payload'}"

@app.route('/<model>/invoke',methods=["POST"])
def index22(model):
    return 'Invoked'

@app.route('/<model>',methods=["GET"])
def index2(model):
    data = FileDB(BASE_PATH+model).read()
    return json.dumps(data)

